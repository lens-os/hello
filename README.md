# hello

A Hello World program for LENS. Will only run on a Linux `x86_64` system.

## Building

To build, run the `./build.sh` script. You must have a nightly Rust toolchain, because several parts of LENS use `#![feature(...)]` attributes.

## Running

To run, run the executable Cargo generates at `target/debug/hello`. It will print "Hello, world!" and then segfault.
