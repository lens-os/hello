#![no_std]
#![no_main]
extern crate lens_flows;
extern crate lens_files;
use lens_flows::byte::Write;

#[no_mangle]
fn main() {
  let stdout = lens_files::File::from_descriptor(1).expect("Failed to open stdout");
  stdout.write(b"Hello, world!\n").expect("Failed to write stdout.");
}
